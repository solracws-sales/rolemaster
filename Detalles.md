# Tabla básica de información

Información básica sobre los distintos libros incluidos en la colección. he creado dos tablas con idéntico formato. la primera con los libros de reglas, ya sean los básicos o alguna expansión (RM Companions, etc), y una segunda con los libros de Shadow World.

## Libros de Reglas.

Ni decir tiene que los libros de reglas básicos son, de lejos, los que más muestran el paso del tiempo el resto está, en general, en muy buen estado.

| ICE [1](#notas) | Título | ISBN | Estado [2](#notas) | Paginas | Notas |
| --------------: | :----- | :--- | :----------------- | :------ | :---- |
| 1000 | RoleMaster Boxed Set | 1-55806-091-X | Bueno | 424 | Caja que contiene los tres manuales "básicos".<br /><ul><li> Arms Law.</li><li>Spell Law.</li><li>Character Law & Campaign Law.</li></ul> |
| 1001 | Rolemaster Combat Screen | 0-915795-13-2 | Nuevo |  12 | Típica "Pantalla del Master" para proteger la intimidad del mismo, incluye 4 páginas de tablas en el lado interior y un libreto de 8 páginas con tablas de críticos, de lejos, la parte más divertida de todo el juego. 😈 |
| 1100 | Arms Law | 1-55806-090-1 | Aceptable | 160 | Reglas para el combate de masas en mundos de _capa y espada_. Un **"Boxed Set"** con todo lo necesario para montar un "War Game" utilizando las reglas de RM. Con fichas de cartón (todavía en su troquel original) para simular una batalla de miles de unidades, unas contra otras. |
| 1110 | War Law | 1-55806-099-5 | Nuevo | 100 | **Libro de Combate** <br/> Reglas de Combate, Tablas de Daño, Tablas de Críticos, etc. | 
| 1200 | Spell Law | 1-55806-092-8 | Usado | 160 | Este volumen es el más acusa el paso del tiempo.<br/>**Libro de Mágia**<br/> Descripción de más de 2000 conjuros, 162 listas de conjuros, tres tipos de mágia, 15 profesiones, tablas de daño y tablas de críticos. |
| 1300 | Character Law & Campaign Law | 1-55806-093-6 | Aceptable | 144 | **Libro de Personajes**<br/> 20 Profesiones y 60 habilidades, sistema de evolución del personaje, reglas sobre curación, heridas, muertes, venenos, hierbas, movimiento, comercio, equipo, etc, etc, etc. |

----

## Shadow World.

Libros donde se describe el mundo fantastico dónde ICE desarrollaba sus aventuras, así como dichas aventuras, todos los libros de aventuras incluyen, en mayor o menor medida, nuevas descripciones para ir detallando, poco a poco, todo ese mundo que se habían inventado. En Negrita los libros de ambientación pura y dura.

| ICE  | Título                             | ISBN          | Estado | Paginas | Notas |
| ---: | :--------------------------------- | :------------ | :----  | :------ | :---- | 
| 6000 | **Shadow World - Master Atlas**    | 1-55806-025-1 | Nuevo  |         | Shadow World: Master Atlas/Boxed Game, incluye: | 
| 6000 | Master Atlas - World Guide         | 1-55806-065-0 | Nuevo  | 64      | Geografía e Historia de Kulthea (El nombre oficial de Shadow World.) |
| 6000 | Master Atlas - Inhabitants Guide   | 1-55806-065-0 | Nuevo  | 64      | Plantas, animales y razas de Kulthea. |
| 6000 | Atlas Addendum                     | N/A           | Nuevo  | 32      | Libreto con descripciones de NPC's, tablas encuentros con animales, plantas y razas exóticas. Mapas del mundo. | 
| 6000 | Mapa de Kulthea                    | N/A           | Aceptable | 1    | Un mapa de 100x120cm a todo color de Kulthea | 
| 6001 | QuellBorne                         | 1-55806-027-8 | Nuevo  | 64      | Primera aventura oficial ambientada en Shadow World, incluye 7 mapas, 3 de ellos a todo color. |
| 6002 | Journey to the Magic Isle          | 1-55806-028-6 | Nuevo  | 64      | Descripción completa de la Univeridad de Artes Mágicas, 7 mapas, 6 ubicaciones para aventuras, 27 NPC's, 14 de ellos listos para ser jugados. |
| 6004 | Tales of the Loremasters           | 1-55806-073-1 | Nuevo  | 32      | Mapas y descripciones de 6 islas, 3 aventuras listas para jugar. |
| 6006 | The orgillion Horror               | 1-55806-029-4 | Nuevo  | 32      | 3 aventuras listas para jugar, 5 ubicaciones detalladas, 3 razas de más allá de este mundo y una nueva lista de conjuros. |
| 6007 | Kingdom of the Desert Jewel        | 1-55806-031-6 | Nuevo  | 64      | 10 ubicaciones detalladas, varias aventuras listas para jugar, 7 Mapas y diagramas 4 de ellos **a color**. |
| 6008 | Tales of the Loremasters - Book II | 1-55806-034-0 | Nuevo  | 32      | 10 scenarios para aventuras con mapas de las zonas montañosas del norte de Jaiman. 1 Aventura lista para ser jugada. | 
| 6009 | Cyclops Vale                       | 1-55806-042-1 | Nuevo  | 32      | Descripción de la ciudad minera de Coronan, 9 mapas y vistas, 6 ubicaciones para aventuras y varias aventuras listas para jugar. |
| 6010 | Jaiman - Land of Twilight          | 1-55806-077-4 | Nuevo  | 96      | 8 aventuras listas para jugar. descripciones en profundidad de las culturas y razas del continente de Jaiman. 4 mapas **a color**, diagramas de edificios, ciudades, etc. | 
| 6100 | **EMER - The Great Continent**     | 1-55806-126-6 | Nuevo  |  | Shadow World: Emer The Lost Continent, incluye : |
| 6100 | EMER                               | 1-55806-126-6 | Nuevo  | 96 | 96 páginas describiendo el gran continente de Emer, su geografía e historia, plantas, animales, razas, gentes y lugares importantes, etc, etc, etc. | 
| 6100 | Atlas Addendum                     | 1-55806-126-6 | Nuevo  | 96 | 96 páginas añadiendo información sobre Kulthea, Dioses, Dioses oscuros y semidioses, tecnología perdida, artefactos, lugares de Poder, materiales mágicos, etc, etc, etc.  |
| 6100 | Mapas                              | 1-55806-126-6 | Nuevo  |  8 | 4 mapas en B/N y 4 mapas a color de lugares relevantes de Kulthea.  | 


----

### Notas

1. [Iron Crown Enterprises](https://es.wikipedia.org/wiki/Iron_Crown_Enterprises) (ICE para los amigos) fue la editorial que publicó todos los libros de reglas, módulos y expansiones de RoleMaster, desde sus inicios hasta su declive a principio de los años 2000.  
Cada libro tenía su código interno ICE, además de su ISBN, etc.
2. Quiero hacer incapié en que esa es mi valoración **personal** del estado del libro. Otra persona podría pensar distinto.