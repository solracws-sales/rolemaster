# Se Vende Colección RoleMaster & Shadow World

Durante años coleccioné y juegué (a partes iguales) a RoleMaster, empezando por el "Pack Básico" el [Boxed Set](./images/small/ICE_1000_RoleMaster_Boxed_Set_01.jpg) hasta llegar a comprar y jugar en Emer, el continente de [Shadow World](./images/small/ICE_6000_Shadow_World_Master_Atlas_01.jpg), pasando por varios "Rolemaster Companions" y algunos "Creatures & Tresaures".

Despues de mucho tiempo, la vida da muchas vueltas y, después de más de 10 años sin jugar ni una sola partida, creo que, por muchos vínculos emocionales que me unan a ese juego, ha llegado el momento de deshacerme de él.

Quisiera que tuviera una nueva vida en las manos de alguien que supiera apreciar todo lo que se puede llegar a hacer con él, alguien que lo juege y que lo utilize. Por ello y, no voy a engañar a nadie, porque creo que financieramente, puede ser más interesante, vendo la colección como una sola pieza, o todo o nada. 🤷

¿Que así me costará más de vender? Si.
¿Qué no tengo prisa? También.

# De que se compone la colección.

_Grosso Modo_ de lo siguiente:

* El RoleMaster [Boxed Set](./images/small/ICE_1000_RoleMaster_Boxed_Set_01.jpg)

  Que contiene lo siguiente:

  * [Arms Law](./images/small/ICE_1100_Arms_law_01.jpg)

    Armas, Armaduras, Artes marciales Básicas.
    Combate.

    Tablas y más tablas de daño, críticos, etc.
  * [Spell Law](./images/small/ICE_1200_Spell_law_01.jpg)

    Magia, Listas de conjuros. Reglas de resolución de combates mágicos.

    La explicación de los tres tipos básicos de mágia en el mudo de RM (RoleMaster)
    
    Tablas de ataque y críticos.

  * [Character Law & Campaign Law](./images/small/ICE_1300_Character_Law_Campaign_Law_01.jpg)

    Reglas para la creación de personajes, y reglas para gestión de campañas. Imprescindible para poder crear personajes para tus aventuras.

* Rolemaster Companions

  Del 1 al 6. Bloque por describir.
  Companions "Extra" como, por ejemplo:

  * Arms Companion 
  * Elemental Companion
  * Spell User's Companion
  * Alchemy Companion
  * Oriental Companion.

* Dos "Creatures & Treasures"
* Dos Cajas con contenido de Shadow World.

  ¿Que qué narices es "Shadow World"? Originalmete RM tenía una fuerte vinculación con el juego de Rol de "El Señor de los Anillos" (MERP, para los amigos), pero la utilización de esa marca registrada le restaba benefícios, así que ICE decidió inventarse su propio mundo dónde ICE (la empresa encargada de publicar RM) decidió ambientar todas sus aventuras y campañas. 

  Hasta dónde yo sé llegaron a publicarse dos cajas con descripciones de dicho mucho, así como varias (más de diez) aventuras o campañas ambientadas en ellos.

  ICE aprovechaba cada aventura para añadir un poco de información sobre su mundo, algo de historia, contexto, etc.
  * [Shadow World Master Atlas](./images/small/ICE_6000_Shadow_World_Master_Atlas_01.jpg)
  * [EMER - The Great Continent](./images/small/ICE_6100_EMER_The_Great_Continent_01.jpg)

* 8 Aventuras / Campañas ambientadas en Shadow World.

  * ICE#6001 - [QuellBourne](./images/small/ICE_6001_QuellBourne_01.jpg)
  * ICE#6002 - [Jorney to the Magic Isle](./images/small/ICE_6002_Journey_to_the_Magic_Isle_01.jpg)
  * ICE#6004 - [Tales of the Loremasters](./images/small/ICE_6004_Tales_of_the_Loremasters_01.jpg)
  * ICE#6006 - [The Orgillion Horror](./images/small/ICE_6006_The_Orgillion_Horror_01.jpg)
  * ICE#6007 - [Kingdom of the Desert Jewel](./images/small/ICE_6007_Kingdom_of_the_Desert_Jewel_01.jpg)
  * ICE#6008 - [Tales of the Loremasters - Book II](./images/small/ICE_6008_Tales_of_the_Loremaster_Book_II_01.jpg)
  * ICE#6009 - [Cyclops Vale](./images/small/ICE_6009_Cyclops_Vale_01.jpg)
  * ICE#6010 - [Jaiman - Land of Twilight](./images/small/ICE_6010_Jaiman_Land_of_Twilight_01.jpg)

* [War Law](./images/small/ICE_1110_War_Law_01.jpg)

  El sistema de combate de masas para RM, con el que puedes representar un combate de 10 enemigos hasta combates de ejercitos de mil combatientes por unidad.

En total, más de 10 aventuras, 4 libros describiendo un mundo entero dónde jugar, un compendio de reglas básicas para jugar y más de 20 expansiones para hacer que tus personajes sean exactamente como tú quieras que sean y no, simplemente, el típico guerrero con cota de malla y espada ancha.

Si quieres que tu personaje sea un Ninja, puedes. Si quieres que tu personaje sea una concubina espia, puedes. Si quieres que tu personaje sea un alquimista loco, puedes. Si quieres que tu personaje sea un paladín con poderes mágicos, puedes. Creo que te haces una idea.